package org.example.dao;

import org.example.model.Animal;
import org.example.model.Food;

import java.sql.*;

public class FoodDaoImpl implements FoodDao{
    private final Connection connection;

    public FoodDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void createTable() throws SQLException {
        Statement statement =connection.createStatement();
        statement.execute("create table if not exists food " +
                "(id integer not null auto_increment, " +
                "name varchar(100), description varchar(100)," +
                "calories_per_100 varchar(100)," +
                "expiration_date date," +
                " primary key (id))");
    }

    @Override
    public void dropTable() throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("drop table animals");

    }

    @Override
    public void create(Food food) throws SQLException{
        PreparedStatement preparedStatement =connection.prepareStatement(
                "insert into food (name, species) values (?,?)");

        preparedStatement.setString(1, food.getName());
        preparedStatement.setString(2, food.getCalories_per_100());
        preparedStatement.execute();
    }
}
