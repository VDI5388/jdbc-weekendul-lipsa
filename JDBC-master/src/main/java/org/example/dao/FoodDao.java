package org.example.dao;

import org.example.model.Animal;
import org.example.model.Food;

import java.sql.SQLException;

public interface FoodDao {
    public void createTable() throws SQLException;


    public void dropTable() throws SQLException;

    public void  create(Food food)throws SQLException;
}
